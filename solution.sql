-- [ACTIVITY] Solution

-- 1. Create an solution.sql file inside s04/activity project and do the following using the music_db database:

    -- a. Find all artists that has letter d in its name.

    SELECT * FROM artists WHERE name LIKE '%d%';

    -- b. Find all songs that has a length of less than 3:50.

    SELECT * FROM songs WHERE length < '3:50';

    -- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

    SELECT albums.album_title, songs.song_name, songs.length
    FROM albums
    JOIN songs ON albums.id = songs.album_id;

    -- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

    SELECT artists.name, albums.album_title
    FROM artists
    JOIN albums ON artists.id = albums.id
    WHERE albums.album_title LIKE '%a%';

    -- e. Sort the albums in Z-A order. (Show only the first 4 records.)

    SELECT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE albums.album_title LIKE '%a%'
    ORDER BY albums.album_title DESC LIMIT 4;

    -- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

    SELECT albums.album_title, songs.song_name, songs.length
    FROM albums JOIN songs ON albums.id = songs.album_id
    ORDER BY albums.album_title DESC;
